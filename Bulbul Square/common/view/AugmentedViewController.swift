//
//  AugmentedViewController.swift
//  Hedef İletişim
//
//  Created by Hüseyin Bülbül on 29.04.2018.
//  Copyright © 2018 mfs. All rights reserved.
//

import UIKit
import Alamofire

class AugmentedViewController: UIViewController {
    func actionSheetAlert(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func alert(title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func openStoryboard(name: String, identifier: String) {
        if let navigation = self.navigationController {
            let dest = UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: identifier)
            navigation.pushViewController(dest, animated: true)
        }else {
            let dest = UIStoryboard(name: name, bundle: nil).instantiateInitialViewController()
            if let d = dest {
                present(d, animated: true, completion: nil)
            }
        }
    }
    
    func openStoryboardInitial(name: String) {
        if let navigation = self.navigationController {
            let dest = UIStoryboard(name: name, bundle: nil).instantiateInitialViewController()
            if let d = dest {
                navigation.pushViewController(d, animated: true)
            }
        }else {
            let dest = UIStoryboard(name: name, bundle: nil).instantiateInitialViewController()
            if let d = dest {
                present(d, animated: true, completion: nil)
            }
        }
    }
    
    func isConnected(completionHandler: @escaping ((Bool) -> Void)) {
        let reach = NetworkReachabilityManager()
        reach?.startListening()
        
        reach?.listener = { status in
            if status == .notReachable || status == .unknown {
                completionHandler(false)
            }else {
                completionHandler(true)
            }
        }
    }
    
    func showNetworkDialog(){
        let alert = UIAlertController(title: "Uyarı", message: "İnternet bağlantısı bulamadık. Lütfen cihazınızın internete bağlı olduğundan emin olun.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Tamam", style: .default, handler: { (action) in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, options: [String: Any](), completionHandler: nil)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
}
