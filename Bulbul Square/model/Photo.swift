//
//  Photo.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation

class Photo: Codable {
    var prefix: String?
    var suffix: String?
    
    public func getUrl() -> URL? {
        if let p = prefix, let s = suffix {
            return URL(string: "\(p)original\(s)")
        }
        return nil
    }
}
