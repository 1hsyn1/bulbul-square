//
//  Venue.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation
import Alamofire

class Location: Codable {
    var lat: Double?
    var lng: Double?
    var crossStreet: String?
    var city: String?
}

class Stats: Codable {
    var usersCount: Int?
}

class Venue: Codable {
//    enum CodingKeys: String, CodingKey {
    
//    }
    
    var id: String?
    var name: String?
    var location: Location?
    var stats: Stats?
    var photo: Photo?
    
    func pullPhotos(completionHandler: @escaping (Bool) -> Void) {
        if let id = self.id {
            let params: Parameters = ["client_id": Urls.clientId,
                                      "client_secret": Urls.clientSecret,
                                      "v":"20180512"]
            ApiManager.request(returnType: PhotoResponse.self, url: Urls.photos(venueId: id), method: .get, parameters: params) { (response) in
                if let result = response.result,
                    let photoResult = result as? PhotoResponse,
                    let photos = photoResult.photos,
                    let items = photos.items,
                    items.count > 0 {
                    self.photo = items[0]
                    completionHandler(true)
                }else {
                    completionHandler(false)
                }
            }
        }
    }
}
