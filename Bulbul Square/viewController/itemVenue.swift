//
//  itemVenue.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

class itemVenue: UITableViewCell {
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbPlace: UILabel!
    @IBOutlet weak var lbRating: UILabel!
    
    func set(venue: Venue) {
        if let name = venue.name {
            lbName.text = name
        }else {
            lbName.text = ""
        }
        
        if let location = venue.location {
            if let street = location.crossStreet {
                lbPlace.text = street
            }else {
                lbPlace.text = location.city
            }
        }else {
            lbPlace.text = ""
        }
        
        if let stats = venue.stats, let count = stats.usersCount {
            lbRating.text = "\(count)"
        }else {
            lbRating.text = ""
        }
    }
}
