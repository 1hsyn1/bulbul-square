//
//  TableVC.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

class TableVC: AugmentedViewController {
    @IBOutlet weak var tvVenues: UITableView!
    
    override func viewDidLoad() {
        tvVenues.register(UINib(nibName: "itemVenue", bundle: nil), forCellReuseIdentifier: "venuereuse")
        tvVenues.reloadData()
    }
}

extension TableVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let v = VenueManager.shared.getVenue(at: indexPath.row) {
            let detail = ViewDetail(frame: self.view.frame, venue: v)
            UIApplication.shared.keyWindow?.addSubview(detail)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VenueManager.shared.count()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvVenues.dequeueReusableCell(withIdentifier: "venuereuse") as! itemVenue
        cell.selectionStyle = .none

        if let v = VenueManager.shared.getVenue(at: indexPath.row) {
            cell.set(venue: v)
        }
        
        return cell
    }
    
    
}
