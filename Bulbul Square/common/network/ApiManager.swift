//
//  ApiManager.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Urls {
    private static let baseUrl = "https://api.foursquare.com/v2/"
    
    public static let clientId = "OTJZS4U3RUQGQHERKZ33OEJYCMRXDWTIJRFY1VYQURBJZJFI"
    public static let clientSecret = "XZVZ5O3RKJTWTKMGLLG03FDKF0ZN25VKZQVGCUZOANIVQN3S"
    public static let search = "\(Urls.baseUrl)venues/search"
    
    public static func photos(venueId: String) -> String {
        return "\(Urls.baseUrl)venues/\(venueId)/photos"
    }
}

class ApiManager {
    public static var shouldLog: Bool = false
    public static var defaultErrorMessage = "An error occured."
    
    private static func authorizationHeaders() -> HTTPHeaders {
        let header : HTTPHeaders = [
            "Content-Type": "application/x-www-form-urlencoded",
//            "Authorization": "Bearer \(token)"
        ]
        return header
    }
    
    
    
    public static func request<T: Decodable>(returnType: T.Type, url: String, method: HTTPMethod, parameters: Parameters?, completionHandler: ((NetworkResponse) -> Void)?){
        let headers: HTTPHeaders = self.authorizationHeaders()
        var encoding = URLEncoding.default
        if method == .post {
            encoding = URLEncoding.httpBody
        }
        
        Alamofire.request(URL(string: url)!, method: method, parameters: parameters, encoding: encoding, headers: headers)
            .responseJSON { (response) in
                let returner = NetworkResponse()
                do {
                    let json = try JSON(data: response.data!)
                    if shouldLog {
                        print(json)
                    }
                    
                    if json["response"] != JSON.null {
                        let decoder = JSONDecoder()
                        returner.result = try decoder.decode(returnType, from: json["response"].rawData())
                        if returner.result != nil {
                            returner.isSuccess = true
                        }else {
                            returner.isSuccess = false
                            returner.errorMessage = defaultErrorMessage
                        }
                    }
                    if let c = completionHandler {
                        c(returner)
                    }
                }catch {
                    returner.isSuccess = false
                    if let c = completionHandler {
                        c(returner)
                    }
                    if shouldLog {
                        print("error at url : \(url)")
                    }
                }
        }
    }
}
