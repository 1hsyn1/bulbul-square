//
//  Loading.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

open class Loading: UIView {
    
    fileprivate static weak var customSuperview: UIView? = nil
    let loadingView = UIView()
    let actInd = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    open class var shared: Loading {
        struct Singleton {
            static let instance = Loading(frame: CGRect(x: 0 ,y: 0, width: deviceWidth, height: deviceHeight))
        }
        return Singleton.instance
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .clear
        self.alpha = 0.0
        loadingView.backgroundColor = .black
        loadingView.alpha = 0.6
        addSubview(loadingView)
        actInd.center = loadingView.center
        actInd.startAnimating()
        addSubview(actInd)
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 1.0
        })
    }
    
    open class func show() {
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.addSubview(shared)
        }
    }
    
    open class func hide() {
        shared.removeFromSuperview()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("fataaal error coder fln")
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if let containerView = Loading.containerView() {
            Loading.shared.frame = containerView.bounds
            self.loadingView.frame = containerView.frame
            self.actInd.center = containerView.center
        }
    }
    
    fileprivate static func containerView() -> UIView? {
        return customSuperview ?? UIApplication.shared.keyWindow
    }
    
}
