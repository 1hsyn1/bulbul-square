//
//  NetworkResponse.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation

class NetworkResponse {
    public var result: Decodable?
    public var isSuccess: Bool = false
    public var errorMessage: String = "Bir hata oluştu."
    
    init(isSuccess: Bool, error: String) {
        self.isSuccess = isSuccess
        errorMessage = error
    }
    
    init() {
        
    }
}

