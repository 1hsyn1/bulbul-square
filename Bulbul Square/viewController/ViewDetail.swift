//
//  ViewDetail.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit
import GoogleMaps
import AlamofireImage

class ViewDetail: UIView {
    //outlets
    @IBOutlet weak var mvMap: GMSMapView!
    @IBOutlet weak var ivImage: UIImageView!
    @IBOutlet weak var aiIndicator: UIActivityIndicatorView!
    
    //variables
    private var venue: Venue?
    
    //actions
    @IBAction func backgroundTouched(_ sender: Any) {
        removeFromSuperview()
    }
    
    //class functions
    init(frame: CGRect, venue: Venue) {
        super.init(frame: frame)
        self.venue = venue
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    fileprivate func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.autoresizingMask = [
            UIViewAutoresizing.flexibleWidth,
            UIViewAutoresizing.flexibleHeight
        ]
        addSubview(view)
        self.alpha = 0
        setAlpha(1, view: self)
        
        if let v = venue {
            if let location = v.location, let lat = location.lat, let lng = location.lng {
                let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 15.0)
                mvMap.camera = camera
                
                let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: lat, longitude: lng))
                if let name = v.name {
                    marker.title = name
                }
                marker.map = mvMap
            }
            
            if let p = v.photo, let url = p.getUrl() {
                ivImage.af_setImage(withURL: url)
            }else {
                ivImage.alpha = 0
                aiIndicator.startAnimating()
                v.pullPhotos { (success) in
                    self.aiIndicator.stopAnimating()
                    if success {
                        if let p = v.photo, let url = p.getUrl() {
                            self.ivImage.image = nil
                            self.ivImage.af_setImage(withURL: url)
                        }else {
                            self.ivImage.image = #imageLiteral(resourceName: "imageNotFound")
                        }
                    }else {
                        self.ivImage.image = #imageLiteral(resourceName: "imageNotFound")
                    }
                    self.ivImage.alpha = 1
                }
            }
        }
    }
    
    
    override func removeFromSuperview() {
        setAlpha(0, view: self) {
            self.removeFromSuperview()
        }
    }
    
    /// Loads a XIB file into a view and returns this view.
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
