//
//  UIViewExtension.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

extension UIView {
    func setAlpha(_ value: CGFloat, view: UIView, completion: (()->())? = nil) {
        UIView.transition(with: view, duration: 0.33, options: .transitionCrossDissolve, animations: {
            view.alpha = value
        }) { (isFin) in
            completion?()
        }
    }
}
