//
//  HomeVC.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

class HomeVC: AugmentedViewController {
    @IBOutlet weak var tfCity: UITextField!
    
    @IBAction func SearchTouched(_ sender: Any) {
        if let text = tfCity.text {
            var city = text
            if city == "" {
                city = "istanbul"
            }
            Loading.show()
            VenueManager.shared.searchVenues(text: city) { (success) in
                Loading.hide()
                if success {
                    self.performSegue(withIdentifier: "totablesegue", sender: nil)
                }else {
                    self.alert(title: "Uyarı", text: "Bir hata oluştu")
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isConnected(completionHandler: { (connected) in
            if !connected {
                self.showNetworkDialog()
            }
        })
    }
}
