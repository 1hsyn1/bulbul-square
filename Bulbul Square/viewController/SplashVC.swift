//
//  ViewController.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import UIKit

class SplashVC: AugmentedViewController {

    override func viewDidAppear(_ animated: Bool) {
        deviceWidth = Int(self.view.bounds.width)
        deviceHeight = Int(self.view.bounds.height)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            self.openStoryboardInitial(name: "Home")
        }
    }


}

