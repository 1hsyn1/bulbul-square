//
//  PhotoResponse.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation
class Photos: Codable {
    var items: [Photo]?
}

class PhotoResponse: Codable {
    var photos: Photos?
}
