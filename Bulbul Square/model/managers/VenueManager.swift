//
//  VenueManager.swift
//  Bulbul Square
//
//  Created by Hüseyin Bülbül on 12.05.2018.
//  Copyright © 2018 bulbul. All rights reserved.
//

import Foundation
import Alamofire

class VenueManager {
    open class var shared: VenueManager {
        struct Singleton {
            static let shared = VenueManager()
        }
        return Singleton.shared
    }
    
    private var list: [Venue] = [Venue]()
    
    func searchVenues(text: String, completionHandler: @escaping (Bool) -> Void){
        list = [Venue]()
        let params: Parameters = ["near":text,
                                  "client_id": Urls.clientId,
                                  "client_secret": Urls.clientSecret,
                                  "v":"20180512"]
        
        ApiManager.request(returnType: SearchResponse.self, url: Urls.search, method: .get, parameters: params) { (response) in
            if let searchResult = response.result,
                let result = searchResult as? SearchResponse,
                let venues = result.venues{
                
                self.list = venues
                completionHandler(response.isSuccess)
            }else {
                completionHandler(false)
            }
        }
    }
    
    func count() -> Int {
        return list.count
    }
    
    func getVenue(at index: Int) -> Venue? {
        if index > -1, index < list.count {
            return list[index]
        }
        return nil
    }
}
